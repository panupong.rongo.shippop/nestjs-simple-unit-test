import { Controller, Get } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { AppService } from './app.service'

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private configService: ConfigService,
  ) {}

  @Get()
  getHello(): string {
    // console.info(`appMode`, appMode)
    const nodeEnv = this.configService.get<string>('nodeEnv')

    console.info(`Read`, nodeEnv)

    return this.appService.getHello()
  }
}
